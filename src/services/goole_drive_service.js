const fs = require('fs')
const readline = require('readline')
const { google } = require('googleapis')
const { compute } = google.compute('v1')
// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly']
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json'

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
export const authorize = async credentials => {
    const { client_secret, client_id, redirect_uris } = credentials.installed
    const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0])

    return new Promise((resolve, reject) => {
        fs.readFile(TOKEN_PATH, (err, token) => {
            // Check if we have previously stored a token.
            if (err) {
                try {
                    resolve(getAccessToken(oAuth2Client))
                } catch (error) {
                    reject(error)
                }
            }
            oAuth2Client.setCredentials(JSON.parse(token))
            resolve(oAuth2Client)
        })
    })
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(oAuth2Client) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    })
    console.log('Authorize this app by visiting this url:', authUrl)
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    })
    rl.question('Enter the code from that page here: ', code => {
        rl.close()
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error retrieving access token', err)
            oAuth2Client.setCredentials(token)
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), err => {
                if (err) throw err
                console.log('Token stored to', TOKEN_PATH)
            })
            return oAuth2Client
        })
    })
}

/**
 * Lists the folder under dolphin names and IDs.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 * @param {string} rootFolderId - 1TeLIosFA4AgMFnlbsCfOaoPBNC_b2Pmt
 */
export const listFolder = async (auth, rootFolderId = '1TeLIosFA4AgMFnlbsCfOaoPBNC_b2Pmt') => {
    const drive = google.drive({ version: 'v3', auth })
    const parmas = {
        q: `mimeType = 'application/vnd.google-apps.folder' and '${rootFolderId}' in parents and trashed = false`,
        fields: 'nextPageToken, files(id, name)',
    }

    const folders = []

    return new Promise((resolve, reject) => {
        drive.files.list(parmas, (err, res) => {
            if (err) return console.log('The API returned an error: ' + err)
            const files = res.data.files
            if (files.length) {
                console.log('Files:')
                files.map(file => {
                    console.log(`${file.name} (${file.id})`)
                    folders.push({ fileName: file.name, fileId: file.id })
                })
            } else {
                reject('No files found.')
            }
            resolve(folders)
        })
    })
}

/**
 * Lists the names and IDs of up to 10 files.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 * @param {string} auth specific folder Id.
 */
export const listFiles = async (auth, folderId) => {
    const drive = google.drive({ version: 'v3', auth })
    const parmas = {
        q: `mimeType = 'application/vnd.google-apps.file' and '${folderId}' in parents and trashed = false`,
        fields: 'nextPageToken, files(id, name)',
    }

    const files = []

    drive.files.list(parmas, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err)
        const files = res.data.files
        if (files.length) {
            console.log('Files:')
            files.map(file => {
                console.log(`${file.name} (${file.id})`)
            })
        } else {
            console.log('No files found.')
        }
    })

    return files
}
