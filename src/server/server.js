// import path from 'path'
// import express from 'express'

import path from 'path'
import express from 'express'
import fs from 'fs'
import * as GoogleDriveServices from '../services/goole_drive_service'

const app = express()
const port = 3001

app.get('/api/google_drive/list', async (req, res) => {
    // Load client secrets from a local file.
    fs.readFile('/Users/yang/Desktop/own_project/dolphin/src/services/credentials.json', async (err, content) => {
        if (err) return console.log('Error loading client secret file:', err)
        // Authorize a client with credentials, then call the Google Drive API.
        const auth = await GoogleDriveServices.authorize(JSON.parse(content))

        const folders = await GoogleDriveServices.listFolder(auth)
        console.log('ahhhhhhh', folders)
        res.json(folders)
    })
})

app.get('/api/google_drive/access/album/:name', (req, res) => {
    const options = {
        root: 'assets',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true,
        },
    }

    // TODO - access album photo from google drive

    res.sendFile('dummy.png', options, function(err) {
        if (err) {
            res.json({ error: `opps > ${err}` })
            next(err)
        } else {
            console.log('Sent:', fileName)
        }
    })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
