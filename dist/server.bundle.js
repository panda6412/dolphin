/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/server/server.js":
/*!******************************!*\
  !*** ./src/server/server.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! fs */ \"fs\");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _services_goole_drive_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/goole_drive_service */ \"./src/services/goole_drive_service.js\");\nfunction asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }\n\nfunction _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"next\", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"throw\", err); } _next(undefined); }); }; }\n\n// import path from 'path'\n// import express from 'express'\n\n\n\n\nvar app = express__WEBPACK_IMPORTED_MODULE_1___default()();\nvar port = 3001;\napp.get('/api/google_drive/list',\n/*#__PURE__*/\nfunction () {\n  var _ref = _asyncToGenerator(\n  /*#__PURE__*/\n  regeneratorRuntime.mark(function _callee2(req, res) {\n    return regeneratorRuntime.wrap(function _callee2$(_context2) {\n      while (1) {\n        switch (_context2.prev = _context2.next) {\n          case 0:\n            // Load client secrets from a local file.\n            fs__WEBPACK_IMPORTED_MODULE_2___default.a.readFile('/Users/yang/Desktop/own_project/dolphin/src/services/credentials.json',\n            /*#__PURE__*/\n            function () {\n              var _ref2 = _asyncToGenerator(\n              /*#__PURE__*/\n              regeneratorRuntime.mark(function _callee(err, content) {\n                var auth, folders;\n                return regeneratorRuntime.wrap(function _callee$(_context) {\n                  while (1) {\n                    switch (_context.prev = _context.next) {\n                      case 0:\n                        if (!err) {\n                          _context.next = 2;\n                          break;\n                        }\n\n                        return _context.abrupt(\"return\", console.log('Error loading client secret file:', err));\n\n                      case 2:\n                        _context.next = 4;\n                        return _services_goole_drive_service__WEBPACK_IMPORTED_MODULE_3__[\"authorize\"](JSON.parse(content));\n\n                      case 4:\n                        auth = _context.sent;\n                        _context.next = 7;\n                        return _services_goole_drive_service__WEBPACK_IMPORTED_MODULE_3__[\"listFolder\"](auth);\n\n                      case 7:\n                        folders = _context.sent;\n                        console.log('ahhhhhhh', folders);\n                        res.json(folders);\n\n                      case 10:\n                      case \"end\":\n                        return _context.stop();\n                    }\n                  }\n                }, _callee);\n              }));\n\n              return function (_x3, _x4) {\n                return _ref2.apply(this, arguments);\n              };\n            }());\n\n          case 1:\n          case \"end\":\n            return _context2.stop();\n        }\n      }\n    }, _callee2);\n  }));\n\n  return function (_x, _x2) {\n    return _ref.apply(this, arguments);\n  };\n}());\napp.get('/api/google_drive/access/album/:name', function (req, res) {\n  var options = {\n    root: 'assets',\n    dotfiles: 'deny',\n    headers: {\n      'x-timestamp': Date.now(),\n      'x-sent': true\n    } // TODO - access album photo from google drive\n\n  };\n  res.sendFile('dummy.png', options, function (err) {\n    if (err) {\n      res.json({\n        error: \"opps > \".concat(err)\n      });\n      next(err);\n    } else {\n      console.log('Sent:', fileName);\n    }\n  });\n});\napp.listen(port, function () {\n  return console.log(\"Example app listening on port \".concat(port, \"!\"));\n});\n\n//# sourceURL=webpack:///./src/server/server.js?");

/***/ }),

/***/ "./src/services/goole_drive_service.js":
/*!*********************************************!*\
  !*** ./src/services/goole_drive_service.js ***!
  \*********************************************/
/*! exports provided: authorize, listFolder, listFiles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"authorize\", function() { return authorize; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"listFolder\", function() { return listFolder; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"listFiles\", function() { return listFiles; });\nfunction asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }\n\nfunction _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"next\", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"throw\", err); } _next(undefined); }); }; }\n\nvar fs = __webpack_require__(/*! fs */ \"fs\");\n\nvar readline = __webpack_require__(/*! readline */ \"readline\");\n\nvar _require = __webpack_require__(/*! googleapis */ \"googleapis\"),\n    google = _require.google;\n\nvar _google$compute = google.compute('v1'),\n    compute = _google$compute.compute; // If modifying these scopes, delete token.json.\n\n\nvar SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly']; // The file token.json stores the user's access and refresh tokens, and is\n// created automatically when the authorization flow completes for the first\n// time.\n\nvar TOKEN_PATH = 'token.json';\n/**\n * Create an OAuth2 client with the given credentials, and then execute the\n * given callback function.\n * @param {Object} credentials The authorization client credentials.\n * @param {function} callback The callback to call with the authorized client.\n */\n\nvar authorize =\n/*#__PURE__*/\nfunction () {\n  var _ref = _asyncToGenerator(\n  /*#__PURE__*/\n  regeneratorRuntime.mark(function _callee(credentials) {\n    var _credentials$installe, client_secret, client_id, redirect_uris, oAuth2Client;\n\n    return regeneratorRuntime.wrap(function _callee$(_context) {\n      while (1) {\n        switch (_context.prev = _context.next) {\n          case 0:\n            _credentials$installe = credentials.installed, client_secret = _credentials$installe.client_secret, client_id = _credentials$installe.client_id, redirect_uris = _credentials$installe.redirect_uris;\n            oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);\n            return _context.abrupt(\"return\", new Promise(function (resolve, reject) {\n              fs.readFile(TOKEN_PATH, function (err, token) {\n                // Check if we have previously stored a token.\n                if (err) {\n                  try {\n                    resolve(getAccessToken(oAuth2Client));\n                  } catch (error) {\n                    reject(error);\n                  }\n                }\n\n                oAuth2Client.setCredentials(JSON.parse(token));\n                resolve(oAuth2Client);\n              });\n            }));\n\n          case 3:\n          case \"end\":\n            return _context.stop();\n        }\n      }\n    }, _callee);\n  }));\n\n  return function authorize(_x) {\n    return _ref.apply(this, arguments);\n  };\n}();\n/**\n * Get and store new token after prompting for user authorization, and then\n * execute the given callback with the authorized OAuth2 client.\n * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.\n * @param {getEventsCallback} callback The callback for the authorized client.\n */\n\nfunction getAccessToken(oAuth2Client) {\n  var authUrl = oAuth2Client.generateAuthUrl({\n    access_type: 'offline',\n    scope: SCOPES\n  });\n  console.log('Authorize this app by visiting this url:', authUrl);\n  var rl = readline.createInterface({\n    input: process.stdin,\n    output: process.stdout\n  });\n  rl.question('Enter the code from that page here: ', function (code) {\n    rl.close();\n    oAuth2Client.getToken(code, function (err, token) {\n      if (err) return console.error('Error retrieving access token', err);\n      oAuth2Client.setCredentials(token); // Store the token to disk for later program executions\n\n      fs.writeFile(TOKEN_PATH, JSON.stringify(token), function (err) {\n        if (err) throw err;\n        console.log('Token stored to', TOKEN_PATH);\n      });\n      return oAuth2Client;\n    });\n  });\n}\n/**\n * Lists the folder under dolphin names and IDs.\n * @param {google.auth.OAuth2} auth An authorized OAuth2 client.\n * @param {string} rootFolderId - 1TeLIosFA4AgMFnlbsCfOaoPBNC_b2Pmt\n */\n\n\nvar listFolder =\n/*#__PURE__*/\nfunction () {\n  var _ref2 = _asyncToGenerator(\n  /*#__PURE__*/\n  regeneratorRuntime.mark(function _callee2(auth) {\n    var rootFolderId,\n        drive,\n        parmas,\n        folders,\n        _args2 = arguments;\n    return regeneratorRuntime.wrap(function _callee2$(_context2) {\n      while (1) {\n        switch (_context2.prev = _context2.next) {\n          case 0:\n            rootFolderId = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : '1TeLIosFA4AgMFnlbsCfOaoPBNC_b2Pmt';\n            drive = google.drive({\n              version: 'v3',\n              auth: auth\n            });\n            parmas = {\n              q: \"mimeType = 'application/vnd.google-apps.folder' and '\".concat(rootFolderId, \"' in parents and trashed = false\"),\n              fields: 'nextPageToken, files(id, name)'\n            };\n            folders = [];\n            return _context2.abrupt(\"return\", new Promise(function (resolve, reject) {\n              drive.files.list(parmas, function (err, res) {\n                if (err) return console.log('The API returned an error: ' + err);\n                var files = res.data.files;\n\n                if (files.length) {\n                  console.log('Files:');\n                  files.map(function (file) {\n                    console.log(\"\".concat(file.name, \" (\").concat(file.id, \")\"));\n                    folders.push({\n                      fileName: file.name,\n                      fileId: file.id\n                    });\n                  });\n                } else {\n                  reject('No files found.');\n                }\n\n                resolve(folders);\n              });\n            }));\n\n          case 5:\n          case \"end\":\n            return _context2.stop();\n        }\n      }\n    }, _callee2);\n  }));\n\n  return function listFolder(_x2) {\n    return _ref2.apply(this, arguments);\n  };\n}();\n/**\n * Lists the names and IDs of up to 10 files.\n * @param {google.auth.OAuth2} auth An authorized OAuth2 client.\n * @param {string} auth specific folder Id.\n */\n\nvar listFiles =\n/*#__PURE__*/\nfunction () {\n  var _ref3 = _asyncToGenerator(\n  /*#__PURE__*/\n  regeneratorRuntime.mark(function _callee3(auth, folderId) {\n    var drive, parmas, files;\n    return regeneratorRuntime.wrap(function _callee3$(_context3) {\n      while (1) {\n        switch (_context3.prev = _context3.next) {\n          case 0:\n            drive = google.drive({\n              version: 'v3',\n              auth: auth\n            });\n            parmas = {\n              q: \"mimeType = 'application/vnd.google-apps.file' and '\".concat(folderId, \"' in parents and trashed = false\"),\n              fields: 'nextPageToken, files(id, name)'\n            };\n            files = [];\n            drive.files.list(parmas, function (err, res) {\n              if (err) return console.log('The API returned an error: ' + err);\n              var files = res.data.files;\n\n              if (files.length) {\n                console.log('Files:');\n                files.map(function (file) {\n                  console.log(\"\".concat(file.name, \" (\").concat(file.id, \")\"));\n                });\n              } else {\n                console.log('No files found.');\n              }\n            });\n            return _context3.abrupt(\"return\", files);\n\n          case 5:\n          case \"end\":\n            return _context3.stop();\n        }\n      }\n    }, _callee3);\n  }));\n\n  return function listFiles(_x3, _x4) {\n    return _ref3.apply(this, arguments);\n  };\n}();\n\n//# sourceURL=webpack:///./src/services/goole_drive_service.js?");

/***/ }),

/***/ 0:
/*!****************************************************!*\
  !*** multi @babel/polyfill ./src/server/server.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! @babel/polyfill */\"@babel/polyfill\");\nmodule.exports = __webpack_require__(/*! ./src/server/server.js */\"./src/server/server.js\");\n\n\n//# sourceURL=webpack:///multi_@babel/polyfill_./src/server/server.js?");

/***/ }),

/***/ "@babel/polyfill":
/*!**********************************!*\
  !*** external "@babel/polyfill" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/polyfill\");\n\n//# sourceURL=webpack:///external_%22@babel/polyfill%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fs\");\n\n//# sourceURL=webpack:///external_%22fs%22?");

/***/ }),

/***/ "googleapis":
/*!*****************************!*\
  !*** external "googleapis" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"googleapis\");\n\n//# sourceURL=webpack:///external_%22googleapis%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "readline":
/*!***************************!*\
  !*** external "readline" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"readline\");\n\n//# sourceURL=webpack:///external_%22readline%22?");

/***/ })

/******/ });