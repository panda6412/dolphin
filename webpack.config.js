const path = require('path')
const nodeExternals = require('webpack-node-externals')

module.exports = {
    entry: ['@babel/polyfill', './src/server/server.js'],
    target: 'node',
    output: {
        filename: 'server.bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            {
                test: /\.(js)$/,
                include: __dirname,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
        ],
    },
}
